local mod = {}
mod.name = minetest.get_current_modname()
mod.storage = minetest.get_mod_storage()
-- mod.path = minetest.get_modpath(mod.name) -- I still might need this later

local demo = false

--[[
-- This was useful for debugging... I'll keep it here for later...
local function print_table(t, label)
   label = label or ""
   local tbl = ""
   if t ~= nil then
      for key, val in pairs(t) do
         tbl = tbl.."\t"..key..": "..tostring(val).."\n"
      end
   else
      tbl="\tnil\n"
   end
   print(label, "{\n"..tbl.."}\n\n")
end
--]]
local function M(msg)
   minetest.display_chat_message(("[%s]: %s"):format(mod.name, msg))
end

local function get_server_id(suffix)
   suffix = suffix or ""
   local info = minetest.get_server_info()
   return ("%s:%s%s"):format(info.address, info.port, suffix)
end

function trim(s)
   return (s:gsub("^%s*(.-)%s*$", "%1"))
end
-- from PiL2 20.4

function merge_tables(primary, secondary)
   for key, val in pairs(secondary) do
      primary[key] = val
   end
   return primary
end

function save_table(key, tbl)
   local serialized = minetest.serialize(tbl)
   mod.storage:set_string(key, serialized)
end

local StorageTable = {
  globalkey = "",
  globalt = {},
  localkey = "",
  localt = {},
}

function StorageTable:new(o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   return o
end

function StorageTable:save()
   save_table(self.localkey, self.localt)
   save_table(self.globalkey, self.globalt)
end

function StorageTable:load()
   local t = {}
   t = self:load_table(self.globalkey)
   self.globalt = merge_tables(self.globalt, t)

   t = {}
   t = self:load_table(self.localkey)
   self.localt = merge_tables(self.localt, t)
end

function StorageTable:load_table(key)
   local serialized = ""
   if mod.storage:contains(key) then
      serialized = mod.storage:get(key)
   end
   local t = minetest.deserialize(serialized) or {}
   return t
end

function StorageTable:merge()
   self:save()
   self:load()
end

function StorageTable:set(key, val)
   self.localt[key] = val
   self:merge()
end

function StorageTable:_gset(key, val)
   self.globalt[key] = val
end

function StorageTable:gset(key, val)
   self:_gset(key, val)
   self:merge()
end

function StorageTable:get(key)
   self:load()
   local ret = self.localt[key]
   if ret == nil then
      ret = self.globalt[key]
   end
   return ret
end


local config = StorageTable:new({
      globalkey = "__global_config",
      localkey = get_server_id("_config"),
      known = {},
      localt = {},
      globalt = {},
})

function config:register(key, description, default)
   self.known[key] = description
   self:_gset(key, default)
end

function config:is_key_known(key)
   return self.known[key] ~= nil
end

function config:describe(key)
   return self.known[key]
end

function config:list_keys()
   local t = {}
   for key in pairs(self.known) do
      table.insert(t, key)
   end
   table.sort(t)
   return table.concat(t, ", ")
end

function config:list()
   local t = {}
   local ret = {}
   for key, val in pairs(self.globalt) do
      t[key] = key.."*: "..tostring(val)
   end
   for key, val in pairs(self.localt) do
      t[key] = key..": "..tostring(val)
   end
   for key, val in pairs(t) do
      table.insert(ret, val)
   end
   table.sort(ret)
   M("Config for this server: "..table.concat(ret, ", "))
end

local ignored = StorageTable:new({
      localkey = get_server_id("_ignores"),
      globalkey = "__global_ignores",
      localt = {},
      globalt = {},
})

function ignored:list()
   names = {}
   for key in pairs(self.localt) do
      table.insert(names, key)
   end
   for key in pairs(self.globalt) do
      table.insert(names, key.."*")
   end
   table.sort(names)
   M(("Ignored players: %s"):format(table.concat(names, ", ")))
end

function ignored:add_local(player)
   self:set(player, true)
   self:merge()
   M(("Player '%s' will be ignored on this server"):format(player))
end

function ignored:remove_local(player)
   self:set(player, nil)
   self:merge()
   M(("Player '%s' will be no longer ignored on this server"):format(player))
end

function ignored:add_global(player)
   self:gset(player, true)
   self:merge()
   M(("Player '%s' will be ignored everywhere"):format(player))
end

function ignored:remove_global(player)
   self:gset(player, nil)
   self:merge()
   M(("Player '%s' will be no longer ignored everywhere"):format(player))
end

function ignored:is_ignored(player)
   local loc = self.localt[player]
   local glob = self.globalt[player]

   local ret = loc or glob
   if config:get("reuse_local_as_whitelist") then
      -- if reusing is allowed, remove cases when
      -- player is put on both lists
      ret = ret and not (loc and glob)
   end
   return ret
end

function ignored:toggle()
   local prev = config:get("mod_active")
   config:set("mod_active", not prev)
   local curr
   if prev then
      curr = "off"
   else
      curr = "on"
   end
   config:merge()
   M(("Ignore is now turned %s on this server."):format(curr))
end

function ignored:ison()
   local ret = config:get("mod_active")
   if ret == nil then ret = true end
   return ret
end

local handlers = {}

function handlers:demo(name, msgtype)
   M(("Suppressing %s message by %s"):format(msgtype, name))
end

function handlers:join(name, msg)
   if config:get("hide_all_join") then
      return true
   end

   return ignored:is_ignored(name) and config:get("hide_join")
end

function handlers:normal(name, msg)
   local isupper = (msg == msg:upper())
   local islower = (msg == msg:lower()) -- messages without letters have isupper = true, this will filter them out
   local iscaps = isupper and (not islower)
   local isdummyintro = (name:lower():trim() == msg:lower():trim())
   if iscaps and config:get("ignore_all_caps") then
      return true
   elseif isdummyintro and config:get("ignore_dummy_introduce") then
      return true
   else
      return ignored:is_ignored(name)
   end
end

function handlers:direct(name, msg)
   return ignored:is_ignored(name)
end

local cmd = {}

cmd.list = {
   params = "",
   description = "Lists all currently ignored players. Players marked with * are ignored in global mode.",
   func = function() ignored:list() end
}
cmd.add = {
   params = "<player>",
   description = "Adds <player> to locally ignored nicknames.",
   func = function(plr) ignored:add_local(plr) end
}
cmd.remove = {
   params = "<player>",
   description = "Removes <player> from locally ignored nicknames.",
   func = function(plr) ignored:remove_local(plr) end
}
cmd.gadd = {
   params = "<player>",
   description = "Adds <player> to globally ignored nicknames.",
   func = function(plr) ignored:add_global(plr) end
}
cmd.gremove = {
   params = "<player>",
   description = "Removes <player> to globally ignored nicknames.",
   func = function(plr) ignored:remove_global(plr) end
}
cmd.toggle = {
   params = "",
   description = "Toggles on/off ignore for this server.",
   func = function() ignored:toggle() end
}
cmd.config = {
   params = "<key> <value>",
   descripton = "Sets configuration values for this addon.",
   func = function(param)
      key, val = param:match("([%w_]+) (%w+)")
      if config:is_key_known(key) and val then
         if val == "clear" then
            config:set(key, nil)
            M(("Setting '%s' cleared in local config."):format(key))
         else
            val = minetest.is_yes(val)
            config:set(key, val)
            M(("%s is now set to %s"):format(key, tostring(val)))
         end
      elseif not config:is_key_known(key) then
         M(("Unknown config key '%s'."):format(key))
      else
         M("Ignore config: bad syntax. Parameters: key value")
      end
   end
}
cmd.gconfig = {
   params = "<key> <value>",
   descripton = "Globally sets configuration values for this addon. Local setting overwrites global.",
   func = function(param)
      key, val = param:match("([%w_]+) (%w+)")
      if config:is_key_known(key) and val then
         val = minetest.is_yes(val)
         config:gset(key, val)
         M(("%s is now set to %s (in global config)"):format(key, tostring(val)))
      elseif not config:is_key_known(key) then
         M(("Unknown config key '%s'."):format(key))
      else
         M("Ignore config: bad syntax. Parameters: key value")
      end
   end
}
cmd.helpconfig = {
   params = "(<key>)",
   description = "Lists recognised keys configuring the mod. If key is set, display help for it.",
   func = function(key)
      key = key or ""
      key = trim(key)
      if key ~= "" then
         if config:is_key_known(key) then
            -- display key help
            local msg
            msg = ("%s=%s: %s"):format(key,
                                       tostring(config:get(key)),
                                       config:describe(key))
            M(msg)
         else
            M(("Error: unknown key '%s'"):format(key))
         end
      else -- key empty
         M("Available keys: "..config:list_keys())
      end
   end
}
cmd.listconfig = {
   params="",
   description = "Lists all config regarding this server. Configuration from global namespace is noted by *.",
   func = function()
      config:list()
   end
}
cmd.demo = {
   params = "",
   description = "Toggles demo mode on and off. Demo mode settings is not saved between sessions.",
   func = function()
      demo = not demo
      M("Demo mode: "..tostring(demo))
   end
}
cmd.save = {
   params = "",
   description = "Forces config saving.",
   func = function()
      config:save()
      ignored:save()
      M("Configuration saved.")
   end
}

function C(command)
   return ("%s_%s"):format("ignore", command)
end

for key, val in pairs(cmd) do
   minetest.register_chatcommand(C(key), val)
end

local function handle_ignore(msg)
   if not ignored:ison() then
      return
   end

   local ret
   -- joined/left messages
   local name, text = msg:match('^%*%*%* (%S+) (.*)$')
   if name and text then
      ret = handlers:join(name, text)
      if demo and ret then
         handlers:demo(name, "joined/left")
      end
      return ret
   end
   -- normal messages
   local name, text = msg:match('^<([^>%s]+)>%s+(.*)$')
   if name and text then
      ret = handlers:normal(name, text)
      if demo and ret then
         handlers:demo(name, "normal")
      end
      return ret
   end

   -- prefixed messages
   local prefix, name, text = msg:match('^(%S+)%s+<([^>]+)>%s+(.*)$')
   if name and text then
      ret = handlers:normal(name, text)
      if demo and ret then
         handlers:demo(name, "prefixed")
      end
      return ret
   end

   -- /me messages
   local name, text = msg:match('^%* (%S+) (.*)$')
   if name and text then
      ret = handlers:normal(name, text)
      if demo and ret then
         handlers:demo(name, "*me")
      end
      return ret
   end

   -- /msg messages
   local name, text = msg:match('^PM from (%S+): (.*)$')
   if name and text then
      ret = handlers:direct(name, text)
      if demo and ret then
         handlers:demo(name, "/msg")
      end
      return ret
   end

   -- /tell messages
   local name, text = msg:match('^(%S+) whispers: (.*)$')
   if name and text then
      ret = handlers:direct(name, text)
      if demo and ret then
         handlers:demo(name, "/tell")
      end
      return ret
   end
end

config:register(
   "mod_active",
   "When set to true, mod will be activated. Setting to false will turn mod off..",
   true
)

config:register(
   "hide_join",
   "When set to true, joined/left messages from ignored people will be ignored.",
   true
)

config:register(
   "ignore_dummy_introduce",
   "When set to true, mod will also ignore dummy introduce messages (when Nick only says Nick).",
   false
)

config:register(
   "hide_all_join",
   "When set to true, you will see no player joined/left messages.",
   false
)

config:register(
   "ignore_all_caps",
   "When set to true, will ignore messages written in all caps.",
   false
)

config:register(
   "reuse_local_as_whitelist",
   "When set to true, local ignorelist will also serve as whitelist for global one, i.e. you can un-ignore player on specific server by putting them on global AND local list.",
   false
)

config:load()
config:save()

ignored:load()
ignored:save()

minetest.register_on_receiving_chat_message(handle_ignore)
