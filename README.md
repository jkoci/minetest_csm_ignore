Minetest CSM which allows you to ignore nicknames on per-server or global basis.

Requirements
------------

Tested with client versions:

- [x] 5.2-dev

I'd like to know if you are running this mod on other clients without problems (and if something breaks too)! [forum thread](https://forum.minetest.net/viewtopic.php?f=53&t=24393)

Installation
------------

- Make sure Client Side Mods are enabled in your minetest client (`enable_client_modding` in advanced settings)
- Install the mod as `~/.minetest/clientmods/ignore` (i.e. the folder should contain this readme)
- Make sure `~/.minetest/clientmods/mods.conf` exists and contains

```config
load_mod_ignore = true
```

Usage
-----

* `.ignore_list` lists all ignored nicknames. If the nick is marked by star (such as `Foo*`), this nick is ignored on all servers.
* `.ignore_add NICK` adds NICK to server ignore list.
* `.ignore_remove NICK` removes NICK from server ignore list.
* `.ignore_gadd NICK` adds NICK to global ignore list.
* `.ignore_gremove NICK` removes NICK from global ignore list.
* `.ignore_toggle` pauses/resumes all ignores.
* `.ignore_config KEY VAL` sets mod's internal local config KEY to value VAL
* `.ignore_gconfig KEY VAL` sets mod's internal global config KEY to value VAL
* `.ignore_helpconfig (KEY)` gives you help for configuring the addon. If no KEY is set, help lists all keys. Otherwise help message for KEY will be displayed.
* `.ignore_listconfig` gives you overview about your current server configuration.
* `.ignore_demo` toggles demo mode - messages will be suppressed, but chat will show that message happened. Unlike other settings, this is not persistent and resets to `off` on relogging.
* `.ignore_save` forces saving of all saveable configuration.

Config
------
In addition to single-purpose commands, you can also configure the mod via `.ignore_config` command. Call it from chat with two parameters (key and value), like this: `.ignore_config foo yes` to set foo config to true. Parameter gets filtered by minetest.is_yes to convert to boolean. Strings of `yes`, `true` and `1` get converted to true. Strings of `no`, `false` and `0` get converted to false. String of `clear` can be used in local config to delete key entry.

### Currently known config keys
* `hide_all_join` controls hiding of all "player joined/left" messages. Defaults to `false`
* `hide_join` controls hiding of "player joined/left" messages. Defaults to `true`.
* `ignore_all_caps` allows hiding of MESSAGES WRITTEN IN ALL CAPS. Defaults to `false`.
* `ignore_dummy_introduce` allows hiding of messages that only contains author's nickname. Defaults to `false`.
* `mod_active` controls switching mod on and off. Defaults to `true`.
* `reuse_local_as_whitelist` allows you to locally un-ignore people from global list. Defaults to `false` (ignoring people on local OR global list). If set to true, only players on (local XOR global) list will be ignored.